import * as AWS from "aws-sdk";
import * as uuid from "uuid/v4";
import { getKey } from "./decorators/key-decorator";
import { getTable } from "./decorators/table-decorator";
import { Mapper } from "./mapper";

function configure() {
  const { REGION, DYNAMO_ENDPOINT } = process.env;
  return {
    endpoint: DYNAMO_ENDPOINT,
    region: REGION,
  };
}

function getTableSuffix(): string {
  const { ENV, ENV_REPOSITORY } = process.env;
  if (ENV_REPOSITORY && (ENV_REPOSITORY === "true") && ENV) {
    return "-" + ENV;
  }
  return "";
}

export abstract class CrudRepository<T extends { id: string }> {

  protected mapper: Mapper<T>;
  protected key: string;
  protected table: string;
  protected db: AWS.DynamoDB.DocumentClient;

  constructor(
    private entity: new() => T,
  ) {
    const sample = new entity();
    this.mapper = new Mapper<T>();
    this.key = getKey(sample);
    this.table = `${getTable(sample)}${getTableSuffix()}`;
    this.db = new AWS.DynamoDB.DocumentClient(configure());
  }

  public async findOneById(id: string): Promise<T> {
    const Key = { };
    Key[this.key] = id;
    const data = await this.db.get({
      Key, TableName: this.table,
    }).promise();
    if (data.Item) {
      return this.mapper.fromItem(data.Item, this.entity);
    }
  }

  public async update(Item: T): Promise<T> {
    if (Item.id) {
      await this.db.put({
        Item: this.mapper.toItem(Item),
        TableName: this.table,
      }).promise();
      process.stdout.write(`db:update(${this.table}) => id=${Item.id}\n`);
      return Item;
    }
  }

  public async insert(Item: T): Promise<T> {
    Item.id = uuid();
    await this.db.put({
      Item: this.mapper.toItem(Item),
      TableName: this.table,
    }).promise();
    process.stdout.write(`db:insert(${this.table}) => id=${Item.id}\n`);
    return Item;
  }

  public async delete(Item: T): Promise<void> {
    if (Item.id) {
      const Key = { };
      Key[this.key] = Item.id;
      await this.db.delete({
        Key, TableName: this.table,
      }).promise();
      process.stdout.write(`db:delete(${this.table}) => id=${Item.id}\n`);
    }
  }

  public async findAll(): Promise<T[]> {
    const request = await this.db.scan({
      TableName: this.table,
    }).promise();
    if (request.Count) {
      return request.Items.map((Item) => this.mapper.fromItem(Item, this.entity));
    }
    return [];
  }

}
